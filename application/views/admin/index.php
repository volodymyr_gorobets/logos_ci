<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php 
if(isset($style_data)) {
foreach($style_data->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; 
  
  foreach($style_data->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; } ?>
</head>
<body>
	<div>
		<a href='<?php echo site_url("admin/products")?>'>Products</a> |
		<a style="float:right;" href='<?php echo site_url("admin/logout")?>'>Logout</a>
	</div>
	<div style='height:20px;'></div>  
    <div>
		<?php echo $content; ?>
    </div>
</body>
</html>
