<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends Admin_Controller {

	public function index() {
    $this->data = [ 
        'content' => "Welcome to Admin Panel!"
    ];
		$this->_tpl();
  }
}
