<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}
  
	public function login() {
		if($this->ion_auth->logged_in() && $this->ion_auth->is_admin()) {
			redirect('admin/index');
		}
    $data = ["message" => ""];
    
    if($this->input->post('login')) {
        //validate form input
      $this->form_validation->set_rules('email', 'E-mail', 'required');
      $this->form_validation->set_rules('password','Password', 'required');
      if($this->form_validation->run() == true) {
          $remember = (bool) $this->input->post('remember');

          if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
          {
            if($this->ion_auth->is_admin()) {
                redirect('/admin/index', 'refresh');
            } else {
              $data["message"] = "You must be an admin!";
            }
            
          } else {
            $data["message"] = $this->ion_auth->errors();
          }
      } else {
        $data["message"] = validation_errors();
      }
    }
    $this->load->view("admin/login",$data);
  }
	
	public function logout() {
		$logout = $this->ion_auth->logout();
		redirect("/");
	}
}
