<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

	public function index()
	{
    $crud = new grocery_CRUD();
    $crud->set_table('products');
    $crud->set_subject('product');
    $crud->columns("name","price","image","visible");    
    $crud->display_as('visible','Show on site');
    $crud->required_fields('name','price');
    $crud->field_type('visible','true_false');
    $crud->set_field_upload('image','assets/uploads/products');
    
    $crud->callback_after_upload(array($this,'callback_after_upload'));
    
    $output = $crud->render();
    
		$this->data = [ 
        'content' => $output->output,
        'style_data' => $output
    ];
		$this->_tpl();
	}
  
  function callback_after_upload($uploader_response,$field_info, $files_to_upload) {
    $this->load->library('image_moo');
    //Is only one file uploaded so it ok to use it with $uploader_response[0].
    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
    $file_mini = $field_info->upload_path.'/mini/'.$uploader_response[0]->name; 
    
    $this->image_moo->load($file_uploaded)->resize(600,600)->save($file_uploaded,true);
    $this->image_moo->load($file_uploaded)->resize(100,100)->save($file_mini,true);

    return true;
  }
}
