<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Site_Controller {

	public function index()
	{
		$this->load->model("product_model");
		$featured_products = $this->product_model->getProducts(["featured"=>1], false, 4);
		$new_products = $this->product_model->getProducts(["new"=>1], "id, name, price, image, alias", 4);
				
		$data = [
			"featured" => $featured_products,
			"new" => $new_products
		];
		
		$this->data = [
			"title" => "Home page",
			"content" => $this->load->view("home/content", $data, true)
		];
		$this->_tpl();
	}
}
