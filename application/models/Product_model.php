<?php
class Product_model extends CI_Model {
  
  public function getFeaturedProducts() {
    $this->db->where("visible",1);
    $this->db->where("featured",1);
    $this->db->select("id, name, image, alias");
    $this->db->order_by("name","asc");
    $this->db->limit(4);
    $q = $this->db->get("products");
    $products = $q->result_array();
    return $products;
  }
  
  public function getNewProducts() {
    $this->db->where("visible",1);
    $this->db->where("new",1);
    $this->db->select("id, name, image, alias, price");
    $this->db->order_by("name","asc");
    $this->db->limit(4);
    $q = $this->db->get("products");
    $products = $q->result_array();
    return $products;
  }
  
  public function getProducts($where = false, $select = false, $limit = 10, $offset = 0) {
    $this->db->where("visible",1);
    if($where) {
        foreach($where as $k => $v) {
          $this->db->where($k,$v);
        }
    }
    if($select) {
      $this->db->select($select);
    }
    
    $this->db->limit($limit,$offset);
    $q = $this->db->get("products");
    $products = $q->result_array();
    return $products;
  }
  
  public function getCategoryProducts($category_id, $limit, $offset) {
    $this->db->start_cache();
      $this->db->where("visible",1);
      $this->db->where("category_id",$category_id);
    $this->db->stop_cache();
    $total = $this->db->from("products")->count_all_results();
    
    $this->db->limit($limit,$offset);
    $products = $this->db->get("products")->result_array();
    $this->db->flush_cache();
    foreach($products as $k => $product) {
      $products[$k]["link"] = $this->productLink($product["category_id"],$product["alias"]);
    }
    return ["total" => $total, "products" => $products];
  }
  
  public function productLink($category_id, $alias) {
      $this->db->select("alias");
      $this->db->from("categories");
      $this->db->where("id",$category_id);
      $result = $this->db->get()->row_array();
    
      return base_url("catalog/".$result["alias"]."/".$alias);
  }
  
  
  
}
?>