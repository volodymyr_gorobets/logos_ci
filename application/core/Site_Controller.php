<?php


class Site_Controller extends MY_Controller {
    
    public $data = [];
  
    public $template = "index";
  
    public function __construct() {
        parent::__construct();
    }
  
    public function _tpl() {
      $this->load->view($this->template,$this->data);
    }

}
