<?php


class Admin_Controller extends MY_Controller {
    
    public $data = [];
  
    public $template = "admin/index";
  
    public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
          redirect('admin/login');
        }
        $this->load->library("grocery_CRUD");
    }
  
    public function _tpl() { 
      $this->load->view($this->template,$this->data);
    }

}
