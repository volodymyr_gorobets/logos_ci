<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

}

require(APPPATH.'core/Admin_Controller.php'); // contains some logic applicable only to `admin` controllers
require(APPPATH.'core/Site_Controller.php'); // contains some logic applicable only to `public` controllers
